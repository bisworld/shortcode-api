<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Tochka\JsonRpc\Facades\JsonRpcServer;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/v1/public/jsonrpc', function (Request $request) {
    return JsonRpcServer::handle($request->getContent());
});
