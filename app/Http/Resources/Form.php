<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property string name
 * @property string phone
 * @property string email
 * @property string page_uid
 * @property Carbon created_at
 */
class Form extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'     => $this->name,
            'phone'    => $this->phone,
            'email'    => $this->email,
            'page_uid' => $this->page_uid,
            'created'  => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }
}
