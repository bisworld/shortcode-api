<?php

namespace App\Http\Controllers;

use App\Form;
use App\Http\Resources\Form as FormResource;
use Tochka\JsonRpc\Traits\JsonRpcController;
use Tochka\JsonRpc\Exceptions\RPC\InvalidParametersException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FormController extends Controller
{
    use JsonRpcController;

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return FormResource::collection(Form::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param string $name
     * @param string $phone
     * @param string $email
     * @param string $page_uid
     * @return array
     */
    public function store(string $name, string $phone, string $email, string $page_uid)
    {
        $this->validateStore();

        Form::create([
            'name'  => $name,  'phone'    => $phone,
            'email' => $email, 'page_uid' => $page_uid,
        ]);

        return ['status' => 'success'];
    }

    /**
     * Display the specified resource.
     *
     * @param string $page_uid
     * @return FormResource
     * @throws InvalidParametersException
     */
    public function show(string $page_uid)
    {
        $this->validateShow();

        return new FormResource(
            Form::where('page_uid', $page_uid)->firstOrFail()
        );
    }

    /**
     * @throws InvalidParametersException
     */
    private function validateStore()
    {
        $this->validate([
            'name'     => 'required|string|max:100',
            'phone'    => 'required|string|max:20',
            'email'    => 'required|string|max:100|email',
            'page_uid' => 'required|string|max:25|unique:forms',
        ]);
    }

    /**
     * @throws InvalidParametersException
     */
    private function validateShow()
    {
        $this->validate([
            'page_uid' => 'required|string|max:25|exists:forms',
        ]);
    }
}
