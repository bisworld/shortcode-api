<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name', 'phone', 'email', 'page_uid'
    ];
}
